# Your code goes here.
import json


def openfilm(filename: str) -> any:
    with open('film.JSON') as json_file:
        film = json.load(json_file)
    return film


def count(filename: str) -> int:
    return len(openfilm(filename))


def written_by(filename: str, writer: str) -> list:
    res = []
    films = openfilm(filename)
    for film in films:
        if film['Writer'] == writer:
            res.append(film)
    return res


